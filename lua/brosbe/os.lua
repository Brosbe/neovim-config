--file for OS specific stuff


--windows
if vim.fn.has('win32') == 1 then
    vim.notify('Windows detected.\nHow unfortunate')

    --vim.o.shell = "nu"
    --vim.o.shellcmdflag = "--login --stdin --no-newline -c"
    --vim.o.shellredir = "out+err> %s"
    --vim.o.shellpipe = "| complete | update stderr { ansi strip } | tee { get stderr | save --force --raw %s } | into record"
    --vim.o.shelltemp = false
    --vim.o.shellxescape = ""
    --vim.o.shellxquote = ""
    --vim.o.shellquote = ""


    --keeping the powershell config here just in case I ever need it again

    vim.cmd("let &shell = executable('pwsh') ? 'pwsh -NoLogo' : 'powershell'")
    vim.cmd("let &shellcmdflag = '-NoProfile -NonInteractive -ExecutionPolicy RemoteSigned -Command [Console]::InputEncoding=[Console]::OutputEncoding=[System.Text.UTF8Encoding]::new();$PSDefaultParameterValues[''Out-File:Encoding'']=''utf8'';'")
    vim.cmd("let &shellredir = '2>&1 | %%{ \"$_\" } | Out-File %s; exit $LastExitCode'")
    vim.cmd("let &shellpipe  = '2>&1 | %%{ \"$_\" } | Tee-Object %s; exit $LastExitCode'")
    vim.cmd("set shellquote= shellxquote=")

elseif vim.fn.has("mac") == 1 then
    vim.notify('MacOs detected.\nHow did you end up here, Pal?')
    --just in case I ever find myself on a mac system

elseif vim.fn.has("linux") == 1 then
    vim.notify('linux detected.\nWelcome home, my child')
end
