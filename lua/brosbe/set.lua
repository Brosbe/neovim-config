vim.wo.relativenumber = true
vim.wo.number = true

vim.opt.tabstop = 4
vim.opt.softtabstop = 4
vim.opt.shiftwidth = 4
vim.opt.expandtab = true

vim.opt.scrolloff = 8

vim.opt.foldlevel = 100
vim.opt.foldclose = 'all'

vim.opt.guifont = {"Cascadia Code NF", ":h14"}

vim.api.nvim_create_autocmd({ "FileType" }, {
  callback = function()
    if require("nvim-treesitter.parsers").has_parser() then
      vim.opt.foldmethod = "expr"
      vim.opt.foldexpr = "nvim_treesitter#foldexpr()"
    else
      vim.opt.foldmethod = "syntax"
    end
  end,
})

--credit to WhiteBlackGoose on github for the snippet below
vim.g.dotnet_build_project = function()
    local default_path = vim.fn.getcwd() .. '/'
    if vim.g['dotnet_last_proj_path'] ~= nil then
        default_path = vim.g['dotnet_last_proj_path']
    end
    local path = vim.fn.input('Path to your *proj file', default_path, 'file')
    vim.g['dotnet_last_proj_path'] = path
    local cmd = 'dotnet build -c Debug ' .. path .. ' > nul'
    vim.notify(cmd)
    print('')
    print('Cmd to execute: ' .. cmd)
    local f = os.execute(cmd)
    if f == 0 then
        print('\nBuild: ✔️ ')
    else
        print('\nBuild: ❌ (code: ' .. f .. ')')
    end
end

--edited from function above to store the dll path for a project
vim.g.dotnet_get_dll = function()
    local default_path = vim.fn.getcwd() .. '/'
    if vim.g['dotnet_last_dll_path'] ~= nil then
        default_path = vim.g['dotnet_last_dll_path']
    end
    local path = vim.fn.input('Path to project dll', default_path, 'file')
    vim.g['dotnet_last_dll_path'] = path
    return path
end

vim.g.loaded_ruby_provider = 0
vim.g.loaded_perl_provider = 0
vim.g.snacks_animate = false
