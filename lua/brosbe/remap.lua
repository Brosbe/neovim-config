vim.g.mapleader = " "

--oil
vim.keymap.set("n", "-", "<CMD>Oil<CR>", { desc = "Open parent directory" })

--write and force quit keybinds
vim.keymap.set("n", "<leader>fw", vim.cmd.w, { desc = "Write buffer" })
vim.keymap.set("n", "<leader>fq", function() vim.cmd(":q") end, { desc = "Quit" })

--clipboard yanking and pasting
vim.keymap.set("v", "<leader>y", "\"+y", { desc = "Yank selection to clipboard" } )
vim.keymap.set("n", "<leader>y", "\"+yy", { desc = "Yank line to clipboard" } )
vim.keymap.set("n", "<leader>yiw", "\"+yiw", { desc = "Yank inner word to clipboard" } )
vim.keymap.set("n", "<leader>yiW", "\"+yiW", { desc = "Yank inner WORD to clipboard" } )

vim.keymap.set("n", "<leader>p", "\"+p", { desc = "Put from clipboard" } )
vim.keymap.set("v", "<leader>p", "\"+p", { desc = "Replace selection with clipboard" } )
vim.keymap.set("n", "<leader>P", "\"+P", { desc = "Put from clipboard" } )
vim.keymap.set("v", "<leader>P", "\"+P", { desc = "Replace selection with clipboard" } )

--build keymaps for dotnet projects
vim.keymap.set("n", "<leader>B", function() vim.g.dotnet_build_project() end, { noremap = true })

--opening up some useful menus
vim.keymap.set("n", "<leader>ml", "<CMD>Lazy<CR>", { noremap = true, desc = "Open Lazy" })

function SwitchToColemak()
    vim.keymap.set('n', 'k', 'j', { noremap = true })
    vim.keymap.set('n', 'h', 'k', { noremap = true })
    vim.keymap.set('n', 'j', 'h', { noremap = true })

    vim.keymap.set('v', 'k', 'j', { noremap = true })
    vim.keymap.set('v', 'h', 'k', { noremap = true })
    vim.keymap.set('v', 'j', 'h', { noremap = true })
end
