function ColorTheWorld(color)

	if color == nil then
		color = "tokyonight"

    elseif color == nil and vim.g.current_colorscheme ~= nil then
		color = vim.g.current_colorscheme
	end

	vim.cmd.colorscheme(color)
	vim.g.current_colorscheme = color
	color = nil

--  vim.api.nvim_set_hl(0, "Normal", {bg = "none"})
--  vim.api.nvim_set_hl(0, "NormalFloat", {bg = "none"})
--  vim.api.nvim_set_hl(0, "SignColumn", {bg = "none"})
--  vim.api.nvim_set_hl(0, "LineNr", {bg = "none"})
    vim.api.nvim_set_hl(0, 'FloatBorder', { link = 'Normal' })
end

ColorTheWorld()
