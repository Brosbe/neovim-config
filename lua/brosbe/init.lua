--secret file detection
local secret_check = function()
    require("brosbe.secret")
end
local secret_available = pcall(secret_check)
local missing

if secret_available then
    missing = not require("brosbe.secret").configured
else
    missing = true
end
vim.g.brosbe_secret_missing = missing

--lazy.nvim load script from the wiki
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not (vim.uv or vim.loop).fs_stat(lazypath) then
    local lazyrepo = "https://github.com/folke/lazy.nvim.git"
    local out = vim.fn.system({ "git", "clone", "--filter=blob:none", "--branch=stable", lazyrepo, lazypath })
    if vim.v.shell_error ~= 0 then
        vim.api.nvim_echo({
            { "Failed to clone lazy.nvim:\n", "ErrorMsg" },
            { out,                            "WarningMsg" },
            { "\nPress any key to exit..." },
        }, true, {})
        vim.fn.getchar()
        os.exit(1)
    end
end
vim.opt.rtp:prepend(lazypath)

--setup
require("brosbe.remap")
require("brosbe.set")
require("lazy").setup("plugins", {
    change_detection = {
        notify = false,
    },
})

require("brosbe.colors")
require("brosbe.os")

--notify that secrets file is missing
if missing then
    vim.notify("Missing secret file, some modules may not be loaded", vim.log.levels.WARN)
end

vim.cmd("autocmd BufNewFile,BufRead *.cshtml set filetype=razor")
vim.cmd("autocmd BufNewFile,BufRead *.razor set filetype=razor")
vim.cmd("autocmd BufNewFile,BufRead *.kbd set filetype=swift")
