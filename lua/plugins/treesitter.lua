return {
    'nvim-treesitter/nvim-treesitter',
    build = { ':TSUpdate' },
    event = { "VeryLazy" },
    lazy = true,
    config = function()
        require 'nvim-treesitter.install'.prefer_git = false;
        require 'nvim-treesitter.configs'.setup {
            ensure_installed = { "javascript", "c", "lua", "vim", "vimdoc", "query", "html", "c_sharp", "css", "python" },
            sync_install = false,
            highlight = {
                enable = true,
                additional_vim_regex_highlighting = false,
            },
        }
    end
}
