return {
    "seblj/roslyn.nvim",
    dependencies = {
        "tris203/rzls.nvim",
        "neovim/nvim-lspconfig",
    },
    lazy = true,
    ft = { "cs", "fs", "csproj", "sln" },
    config =
        function()
            require("roslyn").setup {
                config = {
                    on_attach = vim.g.brosbe_on_attach,
                    filewatching = true,
                    args = {
                        '--stdio',
                        '--logLevel=Information',
                        '--extensionLogDirectory=' .. vim.fs.dirname(vim.lsp.get_log_path()),
                        '--razorSourceGenerator=' .. vim.fs.joinpath(
                            vim.fn.stdpath 'data' --[[@as string]],
                            'mason',
                            'packages',
                            'roslyn',
                            'libexec',
                            'Microsoft.CodeAnalysis.Razor.Compiler.dll'
                        ),
                        '--razorDesignTimePath=' .. vim.fs.joinpath(
                            vim.fn.stdpath 'data' --[[@as string]],
                            'mason',
                            'packages',
                            'rzls',
                            'libexec',
                            'Targets',
                            'Microsoft.NET.Sdk.Razor.DesignTime.targets'
                        ),
                    },
                    settings = {
                        ["csharp|inlay_hints"] = {
                            csharp_enable_inlay_hints_for_implicit_object_creation = true,
                            csharp_enable_inlay_hints_for_implicit_variable_types = false,
                            csharp_enable_inlay_hints_for_lambda_parameter_types = true,
                            csharp_enable_inlay_hints_for_types = true,
                            dotnet_enable_inlay_hints_for_indexer_parameters = true,
                            dotnet_enable_inlay_hints_for_literal_parameters = true,
                            dotnet_enable_inlay_hints_for_object_creation_parameters = true,
                            dotnet_enable_inlay_hints_for_other_parameters = true,
                            dotnet_enable_inlay_hints_for_parameters = true,
                            dotnet_suppress_inlay_hints_for_parameters_that_differ_only_by_suffix = true,
                            dotnet_suppress_inlay_hints_for_parameters_that_match_argument_name = true,
                            dotnet_suppress_inlay_hints_for_parameters_that_match_method_intent = true,
                        },
                        ["csharp|code_lens"] = {
                            dotnet_enable_references_code_lens = true,
                        },
                        ["csharp|background_analysis"] = {
                            dotnet_analyzer_diagnostics_scope = "fullSolution",
                            dotnet_compiler_diagnostics_scope = "fullSolution",
                        },
                        ["csharp|completion"] = {
                            dotnet_show_completion_items_from_unimported_namespaces = true,
                            dotnet_show_name_completion_suggestions = true,
                        }
                    },
                    handlers = require('rzls.roslyn_handlers'),
                },
            }
        end
}
