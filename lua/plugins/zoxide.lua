return
{
    "nanotee/zoxide.vim",
    lazy = true,
    event = { "VeryLazy" },
}
