return {
    "mhartington/formatter.nvim",
    lazy = true,
    event = { "BufReadPost", "BufWritePost", "BufNewFile" },
    config = function()
        require("formatter").setup {
            logging = false,
            log_level = vim.log.levels.WARN,
            filetype = {
                cs = {
                    function()
                        return {
                            exe = "dotnet",
                            args = {
                                "csharpier",
                            },
                            stdin = true
                        }
                    end
                },

                -- Use the special "*" filetype for defining formatter configurations on
                -- any filetype
                ["*"] = {
                    -- "formatter.filetypes.any" defines default configurations for any
                    -- filetype
                    require("formatter.filetypes.any").remove_trailing_whitespace,
                    -- Remove trailing whitespace without 'sed'
                    -- require("formatter.filetypes.any").substitute_trailing_whitespace,
                }
            }
        }
    end
}
