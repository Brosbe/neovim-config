return
{
    'saghen/blink.cmp',
    lazy = false, -- lazy loading handled internally
    dependencies = 'rafamadriz/friendly-snippets',

    version = "*",

    ---@module 'blink.cmp'
    ---@type blink.cmp.Config
    opts = {
        keymap = {
            ['<Tab>'] = { 'select_and_accept', 'snippet_forward', 'fallback' },
            ['<S-Tab>'] = { 'snippet_backward', 'fallback' },
            ['<up>'] = { 'select_prev', 'fallback' },
            ['<down>'] = { 'select_next', 'fallback' },
            preset = 'default'
        },

        appearance = {
            nerd_font_variant = 'mono',
        },

        sources = {
            default = { 'lsp', 'path', 'snippets', 'buffer' },
        },

        completion = {
            accept = {
                create_undo_point = true,
                auto_brackets = {
                    enabled = true
                },
            },
            documentation = { window = { border = 'single' } },
            menu = {
                border = 'single',
                draw = {
                    components = {
                        kind_icon = {
                            ellipsis = false,
                            text = function(ctx)
                                local kind_icon, _, _ = require('mini.icons').get('lsp', ctx.kind)
                                return kind_icon
                            end,
                            highlight = function(ctx)
                                local _, hl, _ = require('mini.icons').get('lsp', ctx.kind)
                                return hl
                            end,
                        }
                    }
                }
            }
        },

        signature = { enabled = true },
    },
}
