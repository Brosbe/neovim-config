local mapping = [[<c-_>]]
if vim.g.neovide then
    mapping = [[<c-/>]]
end

return {
    "akinsho/toggleterm.nvim",
    lazy = true,
    config = function()
        require("toggleterm").setup {
            open_mapping = mapping,
            direction = 'float',
        }
    end,
    keys = {
        { mapping, "<CMD>ToggleTerm", desc = "Open ToggleTerm" }
    }
}
