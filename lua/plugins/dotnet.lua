return {
    'MoaidHathot/dotnet.nvim',
    ft = "cs",
    cmd = "DotnetUI",
    opts = {
        bootstrap = {
            auto_bootstrap = true,
        },
    },
    keys = {
        { '<leader>d', '<CMD>DotnetUI<CR>', desc = "dotnet"},
        { '<leader>db', '<CMD>DotnetUI file bootstrap<CR>', desc = "bootstrap"},
    }
}
