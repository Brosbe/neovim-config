return {
    'stevearc/oil.nvim',
    opts = {},
    config = function()
        require("oil").setup {
            default_file_explorer = true
        }
    end,
    dependencies = { {"echasnovski/mini.icons", opts = {}} },
}
