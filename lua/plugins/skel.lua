return {
    "motosir/skel-nvim",
    event = "BufAdd",
    lazy = true,
    config = function()
        local skeld = require("skel-nvim")
        skeld.setup {
            mappings = {
                ['*.html'] = "html.skel",
                ['*.js']   = "js.skel",
            },
            substitutions = {
                ['FILENAME']        = skeld.get_filename,
                ['NAME']            = skeld.get_author,
                ['DATE']            = skeld.get_date,
                ['CLASS_NAME']      = skeld.get_classname2,
                ['NAMESPACE_OPEN']  = skeld.get_namespaceopen,
                ['NAMESPACE_CLOSE'] = skeld.get_namespaceclose,
            }
        }
    end
}
