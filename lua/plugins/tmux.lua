if (vim.fn.has("macunix") + vim.fn.has("linux")) > 0 then
    return {
        "aserowy/tmux.nvim",
        lazy = true,
        event = "VeryLazy",
        config = function()
            require("tmux").setup()
        end
    }
else
    return{}
end
