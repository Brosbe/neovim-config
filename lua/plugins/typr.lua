return {
    'nvzone/typr',
    branch = 'dev',
    dependencies = { 'nvzone/volt' },
    opts = {
        kblayout = "colemak"
    }
}
