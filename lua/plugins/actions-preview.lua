return {
    "aznhe21/actions-preview.nvim",
    ft = { "cs", "csproj", "cshtml", "html", "css", "lua" },
    lazy = true,
    dependencies = { "nvim-telescope/telescope.nvim" },
    config = function()
        require("actions-preview").setup {
            vim.keymap.set({ "v", "n" }, "<leader>ca", require("actions-preview").code_actions),
            telescope = vim.tbl_extend(
                "force",
                require("telescope.themes").get_ivy(),
                {
                    make_value = nil,

                    make_make_display = nil,
                }
            ),

        }
    end
}
