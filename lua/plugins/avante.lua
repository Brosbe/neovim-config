--Simply don't load the plugin if there's no need to
if vim.g.brosbe_secret_missing then
    return {}
end

return {
    "yetone/avante.nvim",
    lazy = true,
    event = { "BufReadPost", "BufWritePost", "BufNewFile" },
    opts = {
        provider = "ollama",
        auto_suggestions_provider = "ollama",
        vendors = {
            ollama = {
                -- api_key_name = vim.g.apisecret,
                endpoint = require("brosbe.secret").apiurl,
                __inherited_from = "openai",
                model = "qwen2.5-coder:7b",
                api_key_name = "",
                parse_curl_args = function(opts, code_opts)
                    return {
                        url = opts.endpoint .. "/api/chat/completions",
                        headers = {
                            ["Content-Type"] = "application/json",
                            ["Authorization"] = "Bearer " .. require("brosbe.secret").apisecret,
                        },
                        body = {
                            temperature = 0,
                            model = opts.model,
                            messages = require("avante.providers").openai:parse_messages(code_opts),
                            max_tokens = 2048,
                            stream = true,
                        },
                    }
                end,
            },
        },
        behavior = {
            auto_suggestions = true,
        },
    },
    --Yoinked this one from TiancongLx
    build = (function()
      if jit.os == "Windows" then
          return "powershell -NoProfile -File .\\Build.ps1"
      else
          return "make"
      end
    end)(),
    dependencies = {
        "nvim-treesitter/nvim-treesitter",
        "stevearc/dressing.nvim",
        "nvim-lua/plenary.nvim",
        "MunifTanjim/nui.nvim",
        --- The below dependencies are optional,
        "echasnovski/mini.icons", -- or echasnovski/mini.icons
    },
}
