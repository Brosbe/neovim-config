return
{
    "folke/snacks.nvim",
    priority = 1000,
    lazy = false,
    opts = {
        bigfile = { enabled = true },
        dashboard = {
            enabled = true,
            sections = {
                { section = "header" },
                { icon = " ", title = "Keymaps", section = "keys", indent = 2, padding = 1 },
                { icon = " ", title = "Recent Files", section = "recent_files", indent = 2, padding = 1 },
                { icon = " ", title = "Projects", section = "projects", indent = 2, padding = 1 },
                { section = "startup" },
            },
            preset = {
                header = [[
 █████╗ ███████╗██████╗ ███████╗███████╗████████╗    ██████╗ ███████╗
██╔══██╗██╔════╝██╔══██╗██╔════╝██╔════╝╚══██╔══╝   ██╔═══██╗██╔════╝
███████║███████╗██████╔╝█████╗  ███████╗   ██║█████╗██║   ██║███████╗
██╔══██║╚════██║██╔══██╗██╔══╝  ╚════██║   ██║╚════╝██║   ██║╚════██║
██║  ██║███████║██████╔╝███████╗███████║   ██║      ╚██████╔╝███████║
  ╚═╝  ╚═╝╚══════╝╚═════╝ ╚══════╝╚══════╝   ╚═╝       ╚═════╝ ╚══════╝  ]]







            }
        },
        indent = {
            enabled = true,
            animate = {
                enabled = false
            }
        },
        input = { enabled = true },
        notifier = { enabled = true },
        quickfile = { enabled = true },
        statuscolumn =
        {
            enabled = true,
            left = { "mark", "git" },
            right = { "sign", "fold" },
            git = {
                patterns = { "GitSign", "MiniDiffSign" },
            },
            folds = {
                open = true,
            },
        },
        words = {
            enabled = true,
            modes = { "n", "c" }
        },
        terminal = { enabled = false },
        zen = {
            animate = {
                enabled = false
            }
        },
        toggle = {
            which_key = false,
        },
        styles = {
            zen = {
                enter = true,
                fixbuf = false,
                minimal = false,
                width = 200,
                height = 0,
                backdrop = { transparent = true, blend = 40 },
                keys = { q = false },
                zindex = 40,
                wo = {
                    winhighlight = "NormalFloat:Normal",
                },
            }
        }
    },

    keys = {
        { "<leader>z",  function() Snacks.zen() end,                     desc = "Toggle Zen Mode" },
        { "<leader>Z",  function() Snacks.zen.zoom() end,                desc = "Toggle Zoom" },
        { "<leader>.",  function() Snacks.scratch() end,                 desc = "Toggle Scratch Buffer" },
        { "<leader>lg", function() Snacks.lazygit() end,                 desc = "Lazygit" },
        { "]]",         function() Snacks.words.jump(vim.v.count1) end,  desc = "Next Reference",       mode = { "n", "t" } },
        { "[[",         function() Snacks.words.jump(-vim.v.count1) end, desc = "Prev Reference",       mode = { "n", "t" } },
    }
}
