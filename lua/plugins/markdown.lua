return {
    'MeanderingProgrammer/render-markdown.nvim',
    dependencies = { 'nvim-treesitter/nvim-treesitter', 'echasnovski/mini.icons' },
    config = function()
        require('render-markdown').setup {
            latex = { enabled = false },
            file_types = { 'markdown', 'codecompanion', 'Avante' },
            anti_conceal = {
                ignore = {
                    bullet = true,
                    callout = true,
                    check_icon = true,
                    check_scope = true,
                    code_language = true,
                    dash = true,
                    head_icon = true,
                    link = true,
                    quote = true,
                    table_border = true,
                },
            },
            dash = {
                width = 80,
            },
            code = {
                sign = false,
                width = 'block',
                border = 'thick',
                min_width = 80,
                highlight_language = 'LineNr',
                language_name = true,
            },
        }
    end,
    lazy = true,
    ft = { 'markdown', 'codecompanion', 'Avante' },
}
