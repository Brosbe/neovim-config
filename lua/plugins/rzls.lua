return {
    "tris203/rzls.nvim",
    dependencies = {
        "seblj/roslyn.nvim",
        "neovim/nvim-lspconfig",
    },

    lazy = true,
    ft = { "razor", "cshtml" },
    config = function()
        require("rzls").setup {
            capabilities = vim.g.brosbe_capabilites,
            on_attach = vim.g.brosbe_on_attach,
        }
    end
}
