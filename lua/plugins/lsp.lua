return {
    "neovim/nvim-lspconfig",
    cmd = { "LspInfo", "LspInstall", "LspUninstall" },
    lazy = true,
    event = { "BufReadPost", "BufWritePost", "BufNewFile" },
    dependencies = {
        {
            "williamboman/mason.nvim",
            opts = {
                registries = {
                    'github:mason-org/mason-registry',
                    'github:crashdummyy/mason-registry'
                },
            },

            keys = {
                { "<leader>mm", "<CMD>Mason<CR>", desc = "Open mason"},
            }
        },
        "williamboman/mason-lspconfig.nvim",
        "rafamadriz/friendly-snippets",
        "Issafalcon/lsp-overloads.nvim",
    },
    config = function()
        require("mason-lspconfig").setup {
            ensure_installed = { "cssls", "html", "lua_ls", "emmet_ls" },
        }
        local lspconfig = require('lspconfig')

        local capabilities = vim.lsp.protocol.make_client_capabilities()
        capabilities = require('blink.cmp').get_lsp_capabilities(capabilities)
        capabilities = vim.tbl_deep_extend("force", capabilities, {
            textDocument = {
                diagnostic = {
                    dynamicRegistration = true,
                },
            },
        })

        vim.g.brosbe_capabilites = capabilities

        local on_attach = function(client, bufnr)
            -- Mappings to magical LSP functions!
            local bufopts = { noremap = true, silent = true, buffer = bufnr }
            vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, bufopts)
            vim.keymap.set('n', 'gd', vim.lsp.buf.definition, bufopts)
            vim.keymap.set('n', 'K', vim.lsp.buf.hover, bufopts)
            vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, bufopts)
            vim.keymap.set('n', 'gK', vim.lsp.buf.signature_help, bufopts)
            vim.keymap.set('n', '<space>D', vim.lsp.buf.type_definition, bufopts)
            vim.keymap.set('n', '<space>rn', vim.lsp.buf.rename, bufopts)
            vim.keymap.set('n', 'gr', vim.lsp.buf.references, bufopts)
            vim.keymap.set('n', '<space>cf', function() vim.lsp.buf.format { async = true } end, bufopts)
            if client.server_capabilities.signatureHelpProvider then
                require('lsp-overloads').setup(client, {})
            end
            vim.cmd("set noshellslash")
        end

        vim.g.brosbe_on_attach = on_attach



        lspconfig.cssls.setup {
            capabilities = capabilities,
            on_attach = on_attach,
        }

        lspconfig.html.setup {
            capabilities = capabilities,
            on_attach = on_attach,
        }

        lspconfig.emmet_ls.setup {
            capabilities = capabilities,
        }

        lspconfig.ts_ls.setup {
            capabilities = capabilities,
            on_attach = on_attach,
        }

        lspconfig.eslint.setup {
            capabilities = capabilities,
            on_attach = on_attach,
        }

        lspconfig.lua_ls.setup {
            on_attach = on_attach,
            capabilities = capabilities,
            on_init = function(client)
                local path = client.workspace_folders[1].name
                if vim.loop.fs_stat(path .. '/.luarc.json') or vim.loop.fs_stat(path .. '/.luarc.jsonc') then
                    return
                end

                client.config.settings.Lua = vim.tbl_deep_extend('force', client.config.settings.Lua, {
                    runtime = {
                        -- Tell the language server which version of Lua you're using
                        -- (most likely LuaJIT in the case of Neovim)
                        version = 'LuaJiT'
                    },
                    -- Make the server aware of Neovim runtime files
                    workspace = {
                        checkThirdParty = false,
                        library = {
                            vim.env.VIMRUNTIME
                            -- Depending on the usage, you might want to add additional paths here.
                            -- "${3rd}/luv/library"
                            -- "${3rd}/busted/library",
                        }
                        -- or pull in all of 'runtimepath'. NOTE: this is a lot slower
                        --library = vim.api.nvim_get_runtime_file("", true)
                    }
                })
            end,
            settings = {
                Lua = {}
            }
        }

        lspconfig.powershell_es.setup {
            capabilities = capabilities,
            on_attach = on_attach,
        }

        lspconfig.marksman.setup {
            capabilities = capabilities,
        }

        lspconfig.arduino_language_server.setup {
            capabilities = capabilities,
            on_attach = on_attach,
        }

        lspconfig.pylsp.setup {
            capabilities = capabilities,
            on_attach = on_attach,
        }
    end
}
