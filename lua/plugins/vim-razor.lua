return {
    "jlcrochet/vim-razor",
    ft = { "cshtml", "razor", "blazor" },
    lazy = true,
}
